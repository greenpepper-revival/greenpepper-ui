(function ($) {
    "use strict";

    angular.module('greenpepper.report').factory('ResultPage', ResultPage);

    ResultPage.$inject = ["$q"];

    function ResultPage($q) {

        function badgePageAsync(page) {
            return $q(function(resolve, reject){
                try {
                    badgePage(page);
                    resolve(page);
                } catch (e) {
                    reject(e);
                }
            });
        }

        function badgePage(page) {
            console.log("page", page);
            page.find('[id*=GP_INCLUDE]').each(function(index, element){
                var $element = $(element);
                var isTitle = (index % 2) === 0;
                var label = $element.attr('id');
                console.log("Processing: " + label);
                var idSwitch = label.substring(label.indexOf('GP_INCLUDE_'));
                $element.attr('data-target', idSwitch);
                if (element.attributes.getNamedItem('onclick') !== null) {
                    element.attributes.removeNamedItem('onclick');
                }
                if (! $element.hasClass('gpinclude')) {
                    if (isTitle) {
                        $element.addClass('gpinclude-link');
                        var $dataTarget = $('#' + idSwitch);
                        var statuses = ['success','error','failure','ignored','skipped'];
                        var index_status = 0;
                        for (index_status = 0; index_status<statuses.length; index_status++) {
                            var status = statuses[index_status];
                            var status_count = $dataTarget.find('[data-gp-status*=' + status + ']').length;
                            console.log([status + '_count', status_count]);
                            if (status_count > 0) {
                                var $count_element = $('<span/>');
                                $count_element.text(status_count);
                                $count_element.addClass('gp-' + status + '-count');
                                $element.before($count_element);
                            }
                        }
                    } else {
                        $element.addClass('gpinclude-block');
                        element.style.removeProperty('display');
                        $element.addClass('hidden');
                    }
                    $element.addClass('gpinclude');
                }
            });
            page.find('.gpinclude').click(function(e){
                var $this = $(this);
                var dataTargetId = $this.attr('data-target');
                var $dataTarget = $('#' + dataTargetId);
                if ($dataTarget.hasClass('hidden')) {
                    $dataTarget.removeClass('hidden');
                } else {
                    $dataTarget.addClass('hidden');
                }
                e.preventDefault();
                e.stopPropagation();
            });
        }

        return {
            badgePageAsync: badgePageAsync,
            badgePage: badgePage
        }

    }

})(jQuery);
