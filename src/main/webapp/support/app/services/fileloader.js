(function () {
    "use strict";

    angular.module('greenpepper.services').factory('FileLoader', FileLoader);

    FileLoader.$inject = ['$http'];

    function FileLoader($http) {
        var httpConfig = {cache: false, headers: {'Cache-Control': 'no-cache'}};

        function getlogsurl(repoName, specName) {
            return repoName + '/' + specName + '-output.log';
        }

        function getlog(repoName, specName) {
            return $http.get(getlogsurl(repoName, specName), httpConfig);
        }

        function __endsWith(str, suffix) {
            return str.indexOf(suffix, str.length - suffix.length) !== -1;
        }

        function getpage(repoName, specName) {
            var page = specName;
            if (!__endsWith(specName, ".html")) {
                page = specName + ".html";
            }
            return $http.get(repoName + '/' + page, httpConfig);
        }

        function getindex(repoId) {
            return $http.get(repoId + ".index", httpConfig);
        }

        function getresults(repoId) {
            return $http.get(repoId + ".results", httpConfig);
        }

        function loadreports() {
            return $http.get("index.json", httpConfig);
        }

        return {
            loadreports: loadreports,
            getlog: getlog,
            getpage: getpage,
            getindex: getindex,
            getresults: getresults,
            getlogsurl: getlogsurl
        }
    }

})();